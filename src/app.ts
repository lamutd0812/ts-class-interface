class Department {
  name: string;

  constructor(n: string) {
    this.name = n;
  }

  describe(this: Department) {
    console.log(11111111111, this, `Department: ${this.name}`);
  }
}

const accounting = new Department('Accounting');
accounting.describe(); 

const accountingCopy = { name: 'test', describe: accounting.describe}

accountingCopy.describe();